/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rodoviaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Insercao {
    
    public void inserir(String numVagas, String vagasOcupadas,String numFuncionarios, String taxa, String rua, String cidade, String pais, String telefone, String codigo){
        try {
            Connection c = conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_rodoviaria.rodoviaria(numVagas, vagasOcupadas, numFuncionarios, taxa, rua, cidade, pais, telefone, codigo)values (?,?,?,?,?,?,?,?,?)");
                    ps.setInt(1,  Integer.valueOf(numVagas));
                    ps.setInt(2, Integer.valueOf(vagasOcupadas));
                    ps.setInt(3, Integer.valueOf(numFuncionarios));
                    ps.setFloat(4,Float.valueOf(taxa));
                    ps.setString(5, rua);
                    ps.setString(6, cidade);
                    ps.setString(7, pais);
                    ps.setInt(8, Integer.valueOf(telefone));
                    ps.setString(9, codigo);
                    ps.executeUpdate();
                    c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                
    }
    
    public void inserirOnibus(String placa, String numCarteiraMotorista, String codigo, String numPassageiros, String numPoltrona){
        try {
            Connection c = conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_rodoviaria.onibus(placa, numCarteiraMotorista, codigo, numPassageiros, numPoltrona) values (?,?,?,?,?)");
                    ps.setString(1, placa);
                    ps.setString(2, numCarteiraMotorista);
                    ps.setString(3, codigo);
                    ps.setInt(4,  Integer.valueOf(numPassageiros));
                    ps.setInt(5,  Integer.valueOf(numPoltrona));
                    ps.executeUpdate();
                    c.close();
         } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserir2(String nome, String numcarteiramotorista, String cpf, String idade){
        try {
            Connection c = conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert int cs_rodoviaria.motorista(nome, numcarteiramotorista, cpf, idade) values (?,?,?,?)");
            ps.setString(1, nome);
            ps.setString(2, numcarteiramotorista);
            ps.setString(3, cpf);
            ps.setInt(5,  Integer.valueOf(idade));
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
