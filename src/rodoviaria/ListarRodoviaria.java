package rodoviaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;

public class ListarRodoviaria {
    public void listar(JList listRodoviaria){
        try {
            listRodoviaria.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = conexao.ObterConexao();
            String SQL = "SELECT * FROM sc_rodoviaria.rodoviaria";
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("numVagas"));
                listRodoviaria.setModel(dfm);
                c.close();
            }
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarRodoviaria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void listar2(JList listOnibus){
        try {
            listOnibus.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = conexao.ObterConexao();
            String SQL = "SELECT * FROM sc_rodoviaria.onibus";
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("placa"));
                listOnibus.setModel(dfm);
                c.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListarRodoviaria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void selecaoOnibus(JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p = c.prepareStatement("SELECT * FROM sc_rodoviaria.onibus");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
                numCarteiraMotorista a = new numCarteiraMotorista (rs.getString("numCarteiraMotorista"));
                m.addElement(a); 
            }
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
