/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rodoviaria;

/**
 *
 * @author aluno
 */
public class numCarteiraMotorista {
    private String placa, numCarteiraMotorista, codigo,numPassageiros, numPoltrona;

    public String getPlaca() {
        return placa;
    }

    public String getNumCarteiraMotorista() {
        return numCarteiraMotorista;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNumPassageiros() {
        return numPassageiros;
    }

    public String getNumPoltrona() {
        return numPoltrona;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNumPassageiros(String numPassageiros) {
        this.numPassageiros = numPassageiros;
    }

    public void setNumPoltrona(String numPoltrona) {
        this.numPoltrona = numPoltrona;
    }


    public void setNumCarteiraMotorista(String numCarteiraMotorista) {
        this.numCarteiraMotorista = numCarteiraMotorista;
    }

    public numCarteiraMotorista(String numCarteiraMotorista) {
        this.numCarteiraMotorista = numCarteiraMotorista;
    }

    public numCarteiraMotorista() {
        
    }

    @Override
    public String toString() {
        return numCarteiraMotorista;
    }
    
    
    
}
